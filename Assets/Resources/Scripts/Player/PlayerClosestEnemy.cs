﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClosestEnemy : MonoBehaviour
{
    private GameObject closestEnemy;
    public EnemyMasterController enemyMasterController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        closestEnemy = enemyMasterController.GetClosestEnemy(transform.parent.position);
    }

    public GameObject GetClosestEnemy()
    {
        return closestEnemy;
    }
}
