﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackingBehavior : MonoBehaviour
{
    public GameObject currWeapon;
    public PlayerMovement playerMovement;
    public PlayerClosestEnemy playerClosestEnemy;

    private bool firingWeapon;

    // Update is called once per frame
    void Update()
    {
        if (!playerMovement.IsPlayerMoving())
        {
            if (!firingWeapon && playerClosestEnemy.GetClosestEnemy() != null)
            {
                StartCoroutine(FireWeapon());
            }
        }
        else
        {
            StopCoroutine(FireWeapon());
        }
    }

    IEnumerator FireWeapon()
    {
        firingWeapon = true;
        yield return new WaitForSecondsRealtime(currWeapon.GetComponent<WeaponFramework>().rateOfFire);

        if (!playerMovement.IsPlayerMoving() && playerClosestEnemy.GetClosestEnemy() != null)
        {
          
            GameObject projectileInstate = Instantiate(currWeapon.GetComponent<WeaponFramework>().projectilePrefab, transform.position, Quaternion.identity);

            switch (currWeapon.GetComponent<WeaponFramework>().weaponType)
            {
                case WeaponFramework.WeaponType.projectile:
                    projectileInstate.GetComponent<Rigidbody>().velocity = (playerClosestEnemy.GetClosestEnemy().transform.position - transform.position) * currWeapon.GetComponent<WeaponFramework>().speed;
                    projectileInstate.GetComponent<WeaponProjectile>().dmg = currWeapon.GetComponent<WeaponFramework>().dmg;
                    Destroy(projectileInstate, 2f);
                    break;
            }
        }
        firingWeapon = false;
    }

    //As soon as the player stops moving, immediatly start attacking
    //Current problem is that sometimes it takes the full rate of fire time to fire or start immediatly
}
