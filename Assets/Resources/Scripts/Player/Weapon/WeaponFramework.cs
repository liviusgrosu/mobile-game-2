﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFramework : MonoBehaviour
{
    public float rateOfFire;
    public float dmg;
    public float speed;

    public GameObject projectilePrefab;

    public enum WeaponType
    {
        projectile,
        raycast
    };

    public WeaponType weaponType;
}
