﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponProjectile : MonoBehaviour
{
    public float dmg;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy Body")
        {
            other.GetComponent<EnemyHealth>().RecieveDmg(dmg);
            Destroy(this.gameObject);
        }
    }
}
