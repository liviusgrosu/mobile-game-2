﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed;
    private Rigidbody rb;
    private bool isPlayerMoving;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ReadPlayerInput();
    }

    private void ReadPlayerInput()
    {
        Vector3 dirInput = Vector3.zero;
        float zMove, xMove;
        zMove = xMove = 0f;

        if(Input.GetKey(KeyCode.W))
        {
            zMove = 1f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            xMove = -1f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            zMove = -1f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            xMove = 1f;
        }

        dirInput = new Vector3(xMove, 0, zMove);
        dirInput = dirInput.normalized * movementSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + dirInput);

        isPlayerMoving = (xMove == 0 && zMove == 0) ? false : true;
    }

    public bool IsPlayerMoving()
    {
        return isPlayerMoving;
    }
}
