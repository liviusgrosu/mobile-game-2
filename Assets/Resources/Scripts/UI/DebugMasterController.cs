﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMasterController : MonoBehaviour
{
    private bool debugUIEnabled;
    private Canvas debugCanvas;

    // Start is called before the first frame update
    void Start()
    {
        debugCanvas = GetComponent<Canvas>();

        //ToggleDebugUI(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleDebugUI();
        }
    }

    private void ToggleDebugUI(bool state)
    {
        debugUIEnabled = state;
        debugCanvas.enabled = debugUIEnabled;
    }

    private void ToggleDebugUI()
    {
        debugUIEnabled = !debugUIEnabled;
        debugCanvas.enabled = debugUIEnabled;
    }
}
