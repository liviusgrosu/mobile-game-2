﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMasterController : MonoBehaviour
{
    public List<GameObject> enemies;

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            enemies.Add(obj);
        }
    }

    public void RemoveEnemyFromList(GameObject enemy)
    {
        enemies.Remove(enemy);
    }

    public GameObject GetClosestEnemy(Vector3 playerPos)
    {
        if (enemies.Count == 0) return null;

        GameObject closestEnemy = enemies[0];
        float distToClosestEnemy = Vector3.Distance(enemies[0].transform.position, playerPos);

        foreach (GameObject enemy in enemies)
        {
            float distToCurrEnemy = Vector3.Distance(enemy.transform.position, playerPos);
            if (distToCurrEnemy < distToClosestEnemy)
            {
                closestEnemy = enemy;
                distToClosestEnemy = distToCurrEnemy;
            }
        }

        return closestEnemy;
    }
}
