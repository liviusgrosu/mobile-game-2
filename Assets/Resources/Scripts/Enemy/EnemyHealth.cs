﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    private float startingHP = 100f;
    private float currHP;

    public GameObject healthBarUI;
    private Image healthBarImage;

    private bool takenDamage;

    // Start is called before the first frame update
    void Start()
    {
        currHP = startingHP;

        healthBarImage = healthBarUI.transform.GetChild(1).GetComponent<Image>();

        healthBarUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecieveDmg(float dmg)
    {
        if(!takenDamage)
        {
            takenDamage = true;
            TurnOnHealthBar();
        }

        currHP -= dmg;
        healthBarImage.transform.localScale = new Vector3(15f * (currHP / startingHP), healthBarImage.transform.localScale.y, healthBarImage.transform.localScale.z);
        if (currHP <= 0) Die();
    }

    private void TurnOnHealthBar()
    {
        healthBarUI.SetActive(true);
    }

    private void Die()
    {
        GameObject.Find("Enemy Master Controller").GetComponent<EnemyMasterController>().RemoveEnemyFromList(transform.parent.gameObject);

        Destroy(transform.parent.gameObject);
    }
}
